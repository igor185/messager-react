import React from 'react';
import Chat from './components/Chat/index';
import Spinner from './components/Spinner';

const MESSAGES_URL = 'https://api.myjson.com/bins/1hiqin';

const user = {
    user: "Dave",
    avatar: "https://i.pravatar.cc/300?img=14"
};
const chatName = "myChat";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: null
        };
        this.getMessages();
    }

    getMessages() {
        if (!this.state.messages) {
            fetch(MESSAGES_URL)
                .then(data => data.json())
                .then(data => this.setState({messages: data}))
                .catch(e => document.write(e.message));
        }
    }

    render() {
        return (
            <div>
                {this.state.messages ?
                    (<Chat messages={this.state.messages} user={user} chatName={chatName}
                           sendMessage={(message) => this.sendMessage(message)}
                           deleteMessage={(id) => this.deleteMessage(id)} setLike={(id) => this.setLike(id)}
                           changeMessageText={(id, text) => this.changeMessageText(id, text)}/>)
                    :
                    (<Spinner/>)}
            </div>
        )
    }

    sendMessage(message) {
        this.setState((state) => {
            return {
                messages: [...state.messages, message]
            }
        });
    }

    deleteMessage(id) {
        this.setState((state) => {
            let messages = state.messages;
            let i = findInArray(messages, id, 'id');
            messages.splice(i, 1);
            return {messages}
        });
    }

    setLike(id) {
        this.setState((state) => {
            let messages = state.messages;
            let i = findInArray(messages, id, 'id');
            let reactions = messages[i].reactions;
            if (reactions) {
                let j = findInArray(reactions, user.user, 'user');
                if (j === -1)
                    reactions = [...reactions, user];
                else
                    reactions.splice(j, 1);
            } else
                reactions = [user];
            messages[i].reactions = reactions;
            return {messages}
        });
    }

    changeMessageText(id, text) {
        this.setState((state) => {
            const messages = state.messages;
            const index = findInArray(messages, id, 'id');
            if (index === -1)
                return;
            messages[index].message = text;
            return {messages};
        })
    }
}

export default App;

function findInArray(arr, value, key) {
    for (let i = 0; i < arr.length; i++)
        if (arr[i][key] === value)
            return i;
    return -1;
}
