import React from 'react';
import './style.css';

const default_avatar = "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png";

export default class Avatar extends React.Component {

    render(){
        const url = this.props.url ? this.props.url : default_avatar;
        return (<img src={url} className={'card-img avatar-img'} alt=""/>)
    }
}
