import React from 'react';
import Message from '../Message/index';
import TimeLine from '../TimeLine/index';
import './style.css'

export default class MessagesList extends React.Component {

    componentDidUpdate() {
        const list = document.getElementById('messagesList');
        list.scrollTo(0, list.scrollHeight);
    }

    render() {
        let now = new Date();
        let messages = [...this.props.messages];
        let messages_Time = [];
        let tempDate = new Date();
        for (let i = 0; i < messages.length; i++) {
            let j = new Date(messages[i].created_at);
            if (tempDate && tempDate.getDay() !== j.getDay()) {
                if (j.getDay() === now.getDay())
                    messages_Time.push(<TimeLine time={'Today'} key={j.toLocaleString()}/>);
                else if (j.getDay() + 1 === now.getDay())
                    messages_Time.push(<TimeLine time={'Yesterday'} key={j.toLocaleString()}/>);
                else
                    messages_Time.push(<TimeLine time={j.toLocaleDateString().split(',')[0]}
                                                 key={j.toLocaleString()}/>);
            }
            tempDate = j;
            let elem = messages[i];
            messages_Time.push(<Message key={elem.id} setLike={this.props.setLike} changeMessageText={this.props.changeMessageText} message={elem} user={this.props.user}
                                        deleteMessage={this.props.deleteMessage}/>)
        }

        return (
            <div className={"border messagesList"} id={"messagesList"} style={{height: "calc(100vh - 80px)"}}>
                {messages_Time}
            </div>
        )
    }
}