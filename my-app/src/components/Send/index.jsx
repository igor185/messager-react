import React from 'react';


export default class Send extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    changeValue(e) {
        this.setState({value: e.target.value});
    }

    sendMessage() {
        const message = {
            id: `f${(+new Date()).toString(16)}`,
            created_at: (new Date()).toJSON(),
            message: this.state.value,
            marked_read: false,
            ...this.props.user
        };

        this.setState({value: ''});
        this.props.sendMessage(message);
    }

    render() {
        return (
            <div className={"border"} style={{height:"40px"}}>
                <div className={"input-group"}>
                    <input type="text" className={"form-control"} value = {this.state.value} placeholder={"send message"}
                           onChange={(e) => this.changeValue(e)}/>
                    <div className={"input-group-append"}>
                        <input type={'button'} className={"input-group-text btn btn-primary"} value={"send"}
                               onClick={() => this.sendMessage()}/>
                    </div>
                </div>
            </div>
        )
    }
}