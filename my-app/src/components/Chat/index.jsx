import React from 'react';
import Header from '../Header/index';
import MessagesList from '../MessagesList/index';
import Send from '../Send';

export default class Chat extends React.Component {


    render() {
        return (
            <div className={'container'}>
                <Header name={this.props.chatName} messages={this.props.messages}/>
                <MessagesList messages={this.props.messages} deleteMessage={this.props.deleteMessage} changeMessageText={this.props.changeMessageText} user={this.props.user} setLike={this.props.setLike}/>
                <Send sendMessage={this.props.sendMessage} user={this.props.user}/>
            </div>
        )
    }
}