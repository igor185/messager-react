import React from 'react';
import './style.css'
import Avatar from '../Avatar/index';
import Icon from '../Icon/index';

const delete_src = 'https://img.icons8.com/ios-filled/50/000000/delete-forever.png';
const rewrite_src = 'https://img.icons8.com/ios-filled/50/000000/ball-point-pen.png';
const like_src = 'https://img.icons8.com/ios-filled/50/000000/good-quality.png';

export default class Message extends React.Component {
    constructor(props){
        super(props);
        this.state={
            rewrite: false,
            value: props.message.message
        }
    }

    render() {
        const message = this.props.message;
        let selfPost, icons;
        if (message.user === this.props.user.user) {
            selfPost = 'self-right';
            icons = <div>
                <Icon src={delete_src} callback={() => this.deleteMessage()}/>
                <Icon src={rewrite_src} callback={()=> this.rewrite()}/>
            </div>;
        } else {
            selfPost = 'self-left';
            icons = <div>
                <Icon src={like_src} callback={() => this.setLike()}/>
                {message.reactions ? message.reactions.length : 0}
            </div>;
        }

        return (
            <div className={`message-wrp ${selfPost}`}>
                <div className={'card mb-3'}>
                    <div className={"row no-gutters"}>
                        <div className={"col-md-4 avatar-wrp"}>
                            <Avatar url={message.avatar}/>
                        </div>
                        <div className={"col-md-8"}>
                            <div className={"card-body"}>
                                <h5 className="card-title">{message.user}</h5>
                                <h6 className="card-subtitle mb-2 text-muted">{(new Date(message.created_at)).toLocaleTimeString()}</h6>
                                <div className={"card-text"}>
                                    {this.state.rewrite? <textarea name="" id="" defaultValue={message.message} onChange={(e)=> this.change(e)}/> : message.message}
                                </div>
                                {icons}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    deleteMessage() {
        this.props.deleteMessage(this.props.message.id);
    }
    setLike(){
        this.props.setLike(this.props.message.id);
    }
    rewrite(){
        if(!this.state.rewrite) {
            this.setState({rewrite: true});
            return;
        }
        this.props.changeMessageText(this.props.message.id, this.state.value);
        this.setState({rewrite: false});
    }
    change(e){
        this.setState({value: e.target.value});
    }
}